---
# Display name
title: John Sullivan

# Name pronunciation (optional)
# name_pronunciation: 

# Full name (for SEO)
first_name: John
last_name: Sullivan

# Status emoji
status:
  icon: ☕️

# Is this the primary user of the site?
superuser: true

# Role/position/tagline
role: Postdoctoral researcher

# Organizations/Affiliations to show in About widget
organizations:
- name: ex)situ
  url: https://ex-situ.lri.fr/
- name: LISN
  url: https://www.lisn.upsaclay.fr/
- name: IDMIL (Associate Researcher)
  url: http://idmil.org
- name: Université Paris-Saclay
  url: https://www.universite-paris-saclay.fr/

# Short bio (displayed in user profile at end of posts)
bio: Postdoctoral researcher exploring research through design in the areas of music, movement, dance, and human-computer interaction. 

# Interests to show in About widget
interests:
  - Human-Computer Interaction
  - Music Technology
  - Movement and Embodied Interaction
  - Interface Design
  - Cycling
  - Coffee

# Education to show in About widget
education:
  courses:
    - course: Ph.D. in Music Technology
      institution: McGill University
      year: 2021
    - course: MFA in Intermedia
      institution: University of Maine
      year: 2015
    - course: BFA in Contemporary Music Performance
      institution: College of Santa Fe
      year: 2003

# Skills
# For available icons, see: https://docs.hugoblox.com/getting-started/page-builder/#icons
skills:
  - name: Technical
    items:
      - name: Python
        description: ''
        percent: 80
        icon: pixelfed
        icon_pack: custom
      - name: Data Science
        description: ''
        percent: 100
        icon: chart-line
        icon_pack: fas
      - name: SQL
        description: ''
        percent: 40
        icon: database
        icon_pack: fas
  - name: Hobbies
    color: '#eeac02'
    color_border: '#f0bf23'
    items:
      - name: Hiking
        description: ''
        percent: 60
        icon: person-hiking
        icon_pack: fas
      - name: Cats
        description: ''
        percent: 100
        icon: cat
        icon_pack: fas
      - name: Photography
        description: ''
        percent: 80
        icon: camera-retro
        icon_pack: fas

# Social/Academic Networking
# For available icons, see: https://docs.hugoblox.com/getting-started/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
  - icon: envelope
    icon_pack: fas
    link: 'mailto:johnny@johnnyvenom.com'  # For a direct email link, use "mailto:test@example.org".
    label: Email
  - icon: mastodon
    icon_pack: fab
    link: https://hci.social/@johnnyvenom
    label: Mastodon
    display: 
      header: true
  # - icon: pixelfed
  #   icon_pack: custom
  #   link: https://pixelfed.social/johnnyvenom
  - icon: camera-retro
    icon_pack: fas
    link: https://pixelfed.social/johnnyvenom
  # - icon: instagram
  #   icon_pack: fab
  #   link: https://pixelfed.social/johnnyvenom
  - icon: twitter
    icon_pack: fab
    link: https://twitter.com/jonnyvenom
  - icon: google-scholar
    icon_pack: ai
    link: https://scholar.google.ca/citations?user=3CTPYngAAAAJ
  - icon: gitlab
    icon_pack: fab
    link: https://gitlab.com/johnnyvenom
  - icon: github
    icon_pack: fab
    link: https://github.com/johnnyvenom
    

# Highlight the author in author lists? (true/false)
highlight_name: false
---

John Sullivan is a postdoctoral researcher currently residing in Paris, France. His research focuses on multimodal interaction design and development of new technologies for music, dance and multimedia performance. With a background in music performance and human-computer interaction, his work includes user research, participatory design workshops and collaborations with musicians and performing artists to better understand and support professional performance practices. Additionally he has conducted research in the areas of motion capture analysis of live performance, haptic interaction, and accessible digital musical instrument design.

Under the name Johnny Venom, he has been a part of multiple indie rock groups from the northeastern US. He released several albums with various projects and has toured extensively in the US, Canada, and Europe.

{style="text-align: justify;"}
